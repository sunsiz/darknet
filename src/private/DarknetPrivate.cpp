#include "DarknetPrivate.hpp"

akil::DarknetLib::DarknetPrivate::DarknetPrivate(QObject *parent) : QObject(parent)
{
    srand(2222222);
}

void akil::DarknetLib::DarknetPrivate::init()
{
#ifdef CUDA_AVAILABLE
    cuda_set_device(gpu_index=0);
#else
    gpu_index=-1;
#endif

    _network = parse_network_cfg(_cfgFile.toLatin1().data());
    load_weights(&_network, _weightsFile.toLatin1().data());
    _layer = _network.layers[_network.n - 1];
    set_batch_network(&_network, 1);
}

akil::DarknetLib::DarknetPrivate::~DarknetPrivate()
{}

void akil::DarknetLib::DarknetPrivate::setCfg(QString cfg)
{_cfgFile = cfg;}

void akil::DarknetLib::DarknetPrivate::setWeights(QString weights)
{_weightsFile = weights;}

QVector<cv::Rect> akil::DarknetLib::DarknetPrivate::detect(cv::Mat frame,
                                             QVector<int> classesToBeDetected,
                                             float thresh, float nms)
{
    image im = matToYoloImage(frame);
    rgbgr_image(im);
    image sized = resize_image(im, _network.w, _network.h);

    const int blockSize = _layer.w * _layer.h * _layer.n;
    box *boxes = (box *)calloc(blockSize, sizeof(box));
    float **probs = (float **)calloc(blockSize, sizeof(float *));
    for (int j = 0; j < blockSize; ++j)
    {probs[j] = (float *)calloc(_layer.classes, sizeof(float));}

    float *X = sized.data;
    network_predict(_network, X);
    get_region_boxes(_layer, 1, 1, thresh, probs, boxes, 0, 0,0.5);
    if (nms)
    {do_nms_sort(boxes, probs, blockSize, _layer.classes, nms);}

    QVector<cv::Rect> result;

    for (int i = 0; i < blockSize; ++i)
    {
        const int yoloClass = max_index(probs[i], _layer.classes);

        bool isOneOfWantedClasses = classesToBeDetected.empty() ? true : false;

        for (const int wantedClass : classesToBeDetected)
        {
            if (wantedClass == yoloClass)
            {
                isOneOfWantedClasses = true;
                break;
            }
        }

        if (isOneOfWantedClasses)
        {
            if (probs[i][yoloClass] > thresh)
            {
                box b = boxes[i];

                int left = (b.x - b.w / 2.) * im.w;
                int right = (b.x + b.w / 2.) * im.w;
                int top = (b.y - b.h / 2.) * im.h;
                int bot = (b.y + b.h / 2.) * im.h;

                if(left < 0)
                {left = 0;}
                if(right > im.w - 1)
                {right = im.w - 1;}
                if(top < 0)
                {top = 0;}
                if(bot > im.h - 1)
                {bot = im.h - 1;}

                result.push_back(cv::Rect(left, top, right - left, bot - top));
            }
        }
    }

    free_image(im);
    free_image(sized);
    free(boxes);
    free_ptrs((void **)probs, blockSize);

    return result;
}

image akil::DarknetLib::DarknetPrivate::matToYoloImage(cv::Mat src)
{
    unsigned char *data = (unsigned char *)src.data;
    int h = src.rows;
    int w = src.cols;
    int c = src.channels();
    int step = src.step1();
    image out = make_image(w, h, c);
    int i, j, k, count = 0;

    for (k = 0; k < c; ++k)
    {
        for (i = 0; i < h; ++i)
        {
            for (j = 0; j < w; ++j)
            {out.data[count++] = data[i * step + j * c + k] / 255.;}
        }
    }
    return out;
}

QString akil::DarknetLib::DarknetPrivate::weightsFile() const
{return _weightsFile;}
