#ifndef YOLO_HPP
#define YOLO_HPP

#include "../darknet/network.h"
#include "../darknet/parser.h"
#include "../darknet/region_layer.h"
#include "../darknet/utils.h"
#include "../Darknetlib.hpp"

namespace akil {

class DarknetLib::DarknetPrivate : public QObject
{
    Q_OBJECT
public:
    explicit DarknetPrivate(QObject *parent=0);
    void init();
    virtual ~DarknetPrivate();
public:
    void setCfg(QString cfg);
    void setWeights(QString weights);
public:
    QVector<cv::Rect> detect(cv::Mat frame, QVector<int> classesToBeDetected, float thresh=0.25, float nms=0.4);
    QString weightsFile() const;
private:
    image matToYoloImage(cv::Mat src);
private:
    network _network;
    QString _cfgFile,_weightsFile;
    layer _layer;
};
}
#endif
