#ifndef DARKNETLIB_HPP
#define DARKNETLIB_HPP

#include <akil/VisionUtils.hpp>

#if defined(DARKNET_LIBRARY)
#  define DARKNETLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define DARKNETLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

namespace akil{

class DARKNETLIBSHARED_EXPORT DarknetLib : public QObject
{
    Q_OBJECT
public:
    explicit DarknetLib(QObject *parent=0);
    void init();
    virtual ~DarknetLib();
    static const QString getRevision()
    {
#ifdef CUDA_AVAILABLE
        return "CUDA CommitHash";
#else
        return "OpenCL CommitHash";
#endif
    }
public:
    void setCfg(QString cfg);
    void setWeights(QString weights);
public:
    QVector<cv::Rect> detect(cv::Mat frame, QVector<int> classesToBeDetected, float thresh=0.25, float nms=0.4);
    QString weightsFile() const;
private:
    class DarknetPrivate;
    DarknetPrivate *d=nullptr;
};
}

#endif // DARKNETLIB_HPP
