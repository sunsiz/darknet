#include "private/DarknetPrivate.hpp"

akil::DarknetLib::DarknetLib(QObject *parent) : QObject(parent)
{}

void akil::DarknetLib::init()
{d=new DarknetPrivate();}

akil::DarknetLib::~DarknetLib()
{d->deleteLater();}

void akil::DarknetLib::setCfg(QString cfg)
{
    if(d)
    {d->setCfg(cfg);}
}

void akil::DarknetLib::setWeights(QString weights)
{
    if(d)
    {d->setWeights(weights);}
}

QVector<cv::Rect> akil::DarknetLib::detect(cv::Mat frame, QVector<int> classesToBeDetected, float thresh, float nms)
{
    if(d)
    {
        if(weightsFile().size()>5)
        {
            d->init();
            return d->detect(frame,classesToBeDetected,thresh,nms);
        }
    }
    return QVector<cv::Rect>();
}

QString akil::DarknetLib::weightsFile() const
{
    if(d)
    {return d->weightsFile();}
    {return "";}
}
