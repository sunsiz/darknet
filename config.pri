TEMPLATE = lib
DEFINES += DARKNET_LIBRARY
VERSION = 1.0.0

unix{
    !CUDA_OFF{
        exists("/usr/local/cuda/version.txt") {
            message("FOUND CUDA")
            DEFINES+=CUDA_AVAILABLE
            CONFIG+=CUDA_AVAILABLE

            include(cuda.pri)
        }
    }

    message("Active branch is: "$$system('git symbolic-ref --short HEAD'))
    CONFIG+=c++14
    QMAKE_CFLAGS_DEBUG +=-O0
    QMAKE_CFLAGS_DEBUG +=-g
    QMAKE_CFLAGS_RELEASE +=-Ofast
    QMAKE_CFLAGS_RELEASE +=-O1
    QMAKE_CXXFLAGS_RELEASE += -Ofast
    QMAKE_CXXFLAGS_RELEASE += -O1
    QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter -Wno-unused-function
}
