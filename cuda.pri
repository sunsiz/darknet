SYSTEM_NAME = unix         # Depending on your system either 'Win32', 'x64', or 'Win64'
SYSTEM_TYPE = 64

CUDA_DEFINES += GPU
DEFINES += GPU
CONFIG+=GPU
#DEFINES += OPENCV
#CONFIG+= OPENCV
for(_defines, CUDA_DEFINES):{
    formatted_defines += -D$$_defines
}

CUDA_DEFINES = $$formatted_defines

#CUDA_COMPUTE_ARCH = 52
#for(_a, CUDA_COMPUTE_ARCH):{
#    formatted_arch =$$join(_a,'',' -gencode arch=compute_',',code=sm_$$_a')
#    CUDA_ARCH += $$formatted_arch
#}
CUDA_ARCH= -gencode arch=compute_20,code=[sm_20,sm_21] \
      -gencode arch=compute_30,code=sm_30 \
      -gencode arch=compute_35,code=sm_35 \
      -gencode arch=compute_50,code=[sm_50,compute_50] \
      -gencode arch=compute_52,code=[sm_52,compute_52]


CUDA_SDK= /usr/local/cuda/
INCLUDEPATH += $$CUDA_SDK/include
QMAKE_LIBDIR += $$CUDA_SDK/lib64/
LIBS+= -lcudart -lcufft -lcublas -lcurand
CUDA_OBJECTS_DIR = ${OBJECTS_DIR}
CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')
CUDA_LIBS += $$join(LIBS,'.so ', '', '.so')


# No spaces in path names
CUDA_SOURCES+= \
    $$PWD/src/darknet/activation_kernels.cu \
    $$PWD/src/darknet/avgpool_layer_kernels.cu \
    $$PWD/src/darknet/blas_kernels.cu \
    $$PWD/src/darknet/col2im_kernels.cu \
    $$PWD/src/darknet/convolutional_kernels.cu \
    $$PWD/src/darknet/crop_layer_kernels.cu \
    $$PWD/src/darknet/deconvolutional_kernels.cu \
    $$PWD/src/darknet/dropout_layer_kernels.cu \
    $$PWD/src/darknet/im2col_kernels.cu \
    $$PWD/src/darknet/maxpool_layer_kernels.cu \
    $$PWD/src/darknet/network_kernels.cu
    $$PWD/src/darknet/softmax_layer_kernels.cu
  #  $$PWD/src/darknet/yolo_kernels.cu

NVCC_OPTIONS += --use_fast_math --ptxas-options=-v
CONFIG(debug, debug|release) {
        cuda_d.input = CUDA_SOURCES
        cuda_d.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
        cuda_d.commands = $$CUDA_SDK/bin/nvcc -D_DEBUG $$CUDA_DEFINES --machine $$SYSTEM_TYPE $$CUDA_ARCH -Xcompiler '-fPIC' -c $$NVCC_OPTIONS $$CUDA_INC $$CUDA_LIBS -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME}\
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
        cuda_d.dependency_type = TYPE_C
        QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
        cuda.input = CUDA_SOURCES
        cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
        cuda.commands = $$CUDA_SDK/bin/nvcc $$CUDA_DEFINES  --machine $$SYSTEM_TYPE $$CUDA_ARCH -Xcompiler '-fPIC' -c $$NVCC_OPTIONS $$CUDA_INC $$CUDA_LIBS -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} \
                2>&1 | sed -r \"s/\\(([0-9]+)\\)/:\\1/g\" 1>&2
        cuda.dependency_type = TYPE_C
        QMAKE_EXTRA_COMPILERS += cuda
}
