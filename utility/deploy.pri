UI_DIR = bin/uics
MOC_DIR = bin/mocs
OBJECTS_DIR = bin/objs
UI_HEADERS_DIR=bin/ui
UI_SOURCES_DIR=bin/ui
RCC_DIR=bin/rcc
DESTDIR=bin

CUDA_AVAILABLE{TARGET = darknet-cu}
else{TARGET = darknet-cl}

INSTALLS +=  target
