UI_DIR = bin/uics
MOC_DIR = bin/mocs
OBJECTS_DIR = bin/objs
UI_HEADERS_DIR=bin/ui
UI_SOURCES_DIR=bin/ui
RCC_DIR=bin/rcc
DESTDIR=bin

#CONFIG += console
#CONFIG -= app_bundle
TEMPLATE = app
VERSION = 1.1.0

unix{
    exists("/usr/local/cuda/version.txt") {
        message("FOUND CUDA")
        DEFINES+=CUDA_AVAILABLE
        CONFIG+=CUDA_AVAILABLE

DEFINES += GPU
CONFIG+=GPU
DEFINES += OPENCV
CONFIG+= OPENCV

    }
    CONFIG+=c++14
    QMAKE_CFLAGS_DEBUG +=-O0
    QMAKE_CFLAGS_DEBUG +=-g
    QMAKE_CFLAGS_RELEASE +=-Ofast
    QMAKE_CFLAGS_RELEASE +=-O1
    QMAKE_CXXFLAGS_RELEASE += -Ofast
    QMAKE_CXXFLAGS_RELEASE += -O1
    QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter -Wno-unused-function
}
