include(config.pri)

QT += xml gui widgets

CONFIG(release, debug|release) {

    message("Release Mode")

    unix{
        INCLUDEPATH += /opt/local/include
        LIBS += -L/opt/local/lib -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc
        CUDA_AVAILABLE{LIBS += -L/opt/local/lib/akil -laDarknet-cu}
        else{LIBS += -L/opt/local/lib/akil -laDarknet-cl}
    }
}


CONFIG(debug, debug|release) {

    message("Debug Mode")

    unix{
        INCLUDEPATH += /opt/local/debug/include
        LIBS += -L/opt/local/debug/lib -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc
        CUDA_AVAILABLE{LIBS += -L/opt/local/debug/lib/akil -laDarknet-cu+dbg}
        else{LIBS += -L/opt/local/debug/lib/akil -laDarknet-cl+dbg}
    }
}

SOURCES += \
    $$PWD/src/main.cpp
