#include <akil/Darknetlib.hpp>
#include <QtXml>
#include <QApplication>
#include <thread>

int main(int argc, char **argv)
{
    QApplication app(argc,argv);

    QVector<cv::Rect> result;

    bool failure=false;

    if(argc<6)
    {failure=true;}

    if(!failure)
    {
        QString imageName(argv[1]);
        cv::Mat frame = cv::imread(argv[1]);
        QString weights=QString(argv[2]);
        QString cfgFile=QString(argv[3]);
        double threshold=QString(argv[4]).toDouble();
        double nms=QString(argv[5]).toDouble();
        QVector<int> classesToBeDetected;
        for(int i=6;i<argc;i++)
        {classesToBeDetected<<QString(argv[i]).toInt();}

        akil::DarknetLib _detector;
        _detector.init();
        _detector.setWeights(weights);
        _detector.setCfg(cfgFile);
        result=_detector.detect(frame,classesToBeDetected,threshold,nms);

        if(imageName.contains("test"))
        {
            for (int bb = 0; bb < result.size(); bb++) {
                const cv::Rect rect = result[bb];

                cv::rectangle(frame, rect, cv::Scalar(255, 0,0),3);
            }

            cv::imshow("test", frame);
            cv::waitKey();
        }

        QDomDocument outputXML;
        QDomElement objects = outputXML.createElement("Result");
        outputXML.appendChild(objects);
        for(int i = 0; i < result.size(); i++)
        {
            QDomElement boundingBox = outputXML.createElement("Rectangle");
            boundingBox.setAttribute("x", QString::number(result.at(i).x));
            boundingBox.setAttribute("y", QString::number(result.at(i).y));
            boundingBox.setAttribute("w", QString::number(result.at(i).width));
            boundingBox.setAttribute("h", QString::number(result.at(i).height));
            objects.appendChild(boundingBox);
        }

        std::cout<<outputXML.toString().toStdString();
    }
    else
    {
        std::cout<<"wrong arg list. given:\n";
        for(int i=0;i<argc;i++)
        {std::cout<<argv[i];}
        std::cout<<"\nShould be program image weight cfg threshold nms class class class ...";
    }

    return app.exec();
}
