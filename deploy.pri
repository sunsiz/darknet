UI_DIR = bin/uics
MOC_DIR = bin/mocs
OBJECTS_DIR = bin/objs
UI_HEADERS_DIR=bin/ui
UI_SOURCES_DIR=bin/ui
RCC_DIR=bin/rcc
DESTDIR=bin

unix {

    CONFIG(release, debug|release) {
        CUDA_AVAILABLE{TARGET = aDarknet-cu}
        else{TARGET = aDarknet-cl}
        target.path = /opt/local/lib/akil
        headers.path=/opt/local/include/akil
        headers.files=$$PWD/src/Darknetlib.hpp
    }

    CONFIG(debug, debug|release) {
        CUDA_AVAILABLE{TARGET = aDarknet-cu+dbg}
        else{TARGET = aDarknet-cl+dbg}
        target.path = /opt/local/debug/lib/akil
        headers.path=/opt/local/debug/include/akil
        headers.files=$$PWD/src/Darknetlib.hpp
    }

}

INSTALLS +=  target
INSTALLS +=  headers

DISTFILES += \
    $$PWD/.gitlab-ci.yml
