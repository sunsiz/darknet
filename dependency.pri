include(config.pri)

CONFIG(release, debug|release) {

    message("Release Mode")

    unix{
        INCLUDEPATH += /opt/local/include
        LIBS += -L/opt/local/lib -lopencv_core
    }
}


CONFIG(debug, debug|release) {

    message("Debug Mode")

    unix{
        INCLUDEPATH += /opt/local/debug/include
        LIBS += -L/opt/local/debug/lib  -lopencv_core
    }
}

HEADERS += \
     $$PWD/src/darknet/activation_layer.h \
     $$PWD/src/darknet/activations.h \
     $$PWD/src/darknet/avgpool_layer.h \
     $$PWD/src/darknet/batchnorm_layer.h \
     $$PWD/src/darknet/blas.h \
     $$PWD/src/darknet/box.h \
     $$PWD/src/darknet/classifier.h \
     $$PWD/src/darknet/col2im.h \
     $$PWD/src/darknet/connected_layer.h \
     $$PWD/src/darknet/convolutional_layer.h \
     $$PWD/src/darknet/cost_layer.h \
     $$PWD/src/darknet/crnn_layer.h \
     $$PWD/src/darknet/crop_layer.h \
     $$PWD/src/darknet/cuda.h \
     $$PWD/src/darknet/data.h \
     $$PWD/src/darknet/deconvolutional_layer.h \
     $$PWD/src/darknet/demo.h \
     $$PWD/src/darknet/detection_layer.h \
     $$PWD/src/darknet/dropout_layer.h \
     $$PWD/src/darknet/gemm.h \
     $$PWD/src/darknet/gru_layer.h \
     $$PWD/src/darknet/im2col.h \
     $$PWD/src/darknet/image.h \
     $$PWD/src/darknet/layer.h \
     $$PWD/src/darknet/list.h \
     $$PWD/src/darknet/local_layer.h \
     $$PWD/src/darknet/matrix.h \
     $$PWD/src/darknet/maxpool_layer.h \
     $$PWD/src/darknet/network.h \
     $$PWD/src/darknet/normalization_layer.h \
     $$PWD/src/darknet/option_list.h \
     $$PWD/src/darknet/parser.h \
     $$PWD/src/darknet/region_layer.h \
     $$PWD/src/darknet/reorg_layer.h \
     $$PWD/src/darknet/rnn_layer.h \
     $$PWD/src/darknet/route_layer.h \
     $$PWD/src/darknet/shortcut_layer.h \
     $$PWD/src/darknet/softmax_layer.h \
     $$PWD/src/darknet/stb_image.h \
     $$PWD/src/darknet/stb_image_write.h \
     $$PWD/src/darknet/tree.h \
     $$PWD/src/darknet/utils.h \
    $$PWD/src/Darknetlib.hpp \
    $$PWD/src/private/DarknetPrivate.hpp

SOURCES += \
     $$PWD/src/darknet/activation_layer.c \
     $$PWD/src/darknet/activations.c \
     $$PWD/src/darknet/art.c \
     $$PWD/src/darknet/avgpool_layer.c \
     $$PWD/src/darknet/batchnorm_layer.c \
     $$PWD/src/darknet/blas.c \
     $$PWD/src/darknet/box.c \
     $$PWD/src/darknet/captcha.c \
     $$PWD/src/darknet/cifar.c \
     $$PWD/src/darknet/classifier.c \
     $$PWD/src/darknet/coco.c \
     $$PWD/src/darknet/col2im.c \
     $$PWD/src/darknet/compare.c \
     $$PWD/src/darknet/connected_layer.c \
     $$PWD/src/darknet/convolutional_layer.c \
     $$PWD/src/darknet/cost_layer.c \
     $$PWD/src/darknet/crnn_layer.c \
     $$PWD/src/darknet/crop_layer.c \
     $$PWD/src/darknet/cuda.c \
     $$PWD/src/darknet/data.c \
     $$PWD/src/darknet/deconvolutional_layer.c \
     $$PWD/src/darknet/demo.c \
     $$PWD/src/darknet/detection_layer.c \
     $$PWD/src/darknet/detector.c \
     $$PWD/src/darknet/dice.c \
     $$PWD/src/darknet/dropout_layer.c \
     $$PWD/src/darknet/gemm.c \
     $$PWD/src/darknet/go.c \
     $$PWD/src/darknet/gru_layer.c \
     $$PWD/src/darknet/im2col.c \
     $$PWD/src/darknet/image.c \
     $$PWD/src/darknet/layer.c \
     $$PWD/src/darknet/list.c \
     $$PWD/src/darknet/local_layer.c \
     $$PWD/src/darknet/matrix.c \
     $$PWD/src/darknet/maxpool_layer.c \
     $$PWD/src/darknet/network.c \
     $$PWD/src/darknet/nightmare.c \
     $$PWD/src/darknet/normalization_layer.c \
     $$PWD/src/darknet/option_list.c \
     $$PWD/src/darknet/parser.c \
     $$PWD/src/darknet/region_layer.c \
     $$PWD/src/darknet/reorg_layer.c \
     $$PWD/src/darknet/rnn.c \
     $$PWD/src/darknet/rnn_layer.c \
     $$PWD/src/darknet/rnn_vid.c \
     $$PWD/src/darknet/route_layer.c \
     $$PWD/src/darknet/shortcut_layer.c \
     $$PWD/src/darknet/softmax_layer.c \
     $$PWD/src/darknet/super.c \
     $$PWD/src/darknet/swag.c \
     $$PWD/src/darknet/tag.c \
     $$PWD/src/darknet/tree.c \
     $$PWD/src/darknet/utils.c \
     $$PWD/src/darknet/voxel.c \
     $$PWD/src/darknet/writing.c \
     $$PWD/src/darknet/yolo.c \
    $$PWD/src/Darknetlib.cpp \
    $$PWD/src/private/DarknetPrivate.cpp
